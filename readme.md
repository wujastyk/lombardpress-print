#The LombardPress print style sheet converter tool

As you are working on your TEI transcriptions of a critical edition, it can be really nice to get a nice pdf output of your text as you work. I often put Oxygen on the right side of the screen and my pdf reader on the left, and then let the pdf reader update as I work. 

This tool is designed to automate that transformation. It is the same tool we use to produce our final camera ready proofs You can use the existing .xsl stylesheet (which expects that you are encoding according to the Lbp TEI customization) or you can supply your own. 

This tool also makes a couple of assumptions. 

First, it expects that you have LaTeX installed and that the command `pdflatex` is in your path. If you are using the lbp-latex-critical.xsl style sheet it also assumes you have the required LaTeX packages installed. In particular this includes the LaTeX package [eledmac](https://www.ctan.org/pkg/eledmac) installed.

If you installed LaTeX with [MacTeX](https://tug.org/mactex/) you should be all set.

Second, it assumes that you have [saxon](http://saxon.sourceforge.net/) installed and that it can be accessed in your path with the command `saxon`. If your `saxon` processor responds to a different command, you'll need to modify line 38.

#How to Use

  * Download the repo.

  * Make sure `lbp-convert.sh` is executable by typing `chmod +x lbp-convert.sh`
  
  * Add the `lbp-convert.sh` to your path. 
      
    * Typically this would mean navigating to `/usr/local/bin/`, then typing `ln -s <location of lbp-convert.sh file> lbp-convert`
    
  * Now open 'lbp-convert.sh' and modify line 29 to specify the `base` directory where you want the output of your conversions to be located.
  
  * If you want to use a custom style sheet, change line 69 (?) to reflect the file path of the desired xslt stylesheet.
  
  * Now navigate to the directory of the TEI file you want to convert.
  
  * Run your command lbp-convert <filename without extension> <name of subdirectory you want the conversion directory to be stored in>
  
  * For example:  `lbp-convert lectio1 plaoulcommentary`
  
  * This will navigate to (or create) the directory `plaoulcommentary` inside your specified `base` directory. A directory called `lectio1` will then be created inside `plaoulcommentary` with the associated .tex and .log files. It will also include the desired .pdf file as `lectio1.pdf`.
  
If you use lightweight pdf reader like [Skim](http://skim-app.sourceforge.net/) that can automatically recognize file changes and update, you will have a nice workspace in which your .pdf output automatically updates as you work on your TEI file.

Make sure to stay up to date with this repository, as I hope to make this easier to set up and use in the future. (But probably not the near future :( ) 

# Contributors

  * Jeffrey C. Witt
  * Nicolaus Vaughan

# Version Log 

  * 0.0.1 -- this is the first barebones version, basic functionality works as long as bash, saxon, and pdflatex (with eledmac) package are installed correctly.